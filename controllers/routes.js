const express = require("express");
const path = require("path");
const XeroClient = require("xero-node").AccountingAPIClient;
const fs = require("fs");
const moment = require("moment");

const config = require("../config/config");
const { uploadFilesToGCS } = require("./storage");

const router = express.Router();
let xero = new XeroClient(config);

// const invoiceDocumentPath =
const getFileNameForGoogleCloud = (
  dateString,
  contactName,
  invoiceNumber,
  type,
  status,
  invoiceID,
  docType,
  fileNameFromXero
) => {
  const day = new Date(dateString).getDate().toString();
  const month = ("0" + (new Date(dateString).getMonth() + 1)).slice(-2);
  const year = new Date(dateString).getFullYear().toString();
  const docName = !docType ? `1-Document` : `2-Attachment-${fileNameFromXero}`;

  return `${year}${month}${day} ${contactName} ${invoiceNumber} ${type} ${status} ${invoiceID} ${docName}`;
};

const saveLogFile = (filename, docID, docType, docStatus) => {
  const type = docType === 1 ? `Document` : `Attachments`;
  fs.appendFileSync(
    path.resolve(__dirname, `../bulk/logs/${filename}.txt`),
    `${docID}-${type}-${docStatus}\n`,
    err => {
      if (err) return console.log(err);
    }
  );
  fs.ap
};

const saveDocumentFromXero = async (docType, item, nameForBucket) => {
  if (docType === "creditNotes") {
    await xero.creditNotes.savePDF({
      CreditNoteID: item.CreditNoteID,
      CreditNoteNumber: item.CreditNoteNumber,
      savePath: path.resolve(
        __dirname,
        `../bulk/creditnotes/${nameForBucket}.pdf`
      )
    });
  } else {
    await xero.invoices.savePDF({
      InvoiceID: item.InvoiceID,
      InvoiceNumber: item.InvoiceNumber,
      savePath: path.resolve(__dirname, `../bulk/invoice/${nameForBucket}.pdf`)
    });
  }

  await uploadFilesToGCS(
    `${
      docType === `creditNotes` ? `creditnotes` : `invoice`
    }/${nameForBucket}.pdf`
  );
};

const saveAttachmentsFromXero = async (docType, item) => {
  try {
    const result =
      docType === "creditNotes"
        ? await xero.creditNotes.attachments.get({
            entityId: item.CreditNoteID
          })
        : await xero.invoices.attachments.get({ entityId: item.InvoiceID });

    const attachments = result.Attachments;
    attachments.forEach(async element => {
      const fileName = element.FileName;
      const mimeType = element.MimeType;
      const attachmentNameForBucket = getFileNameForGoogleCloud(
        item.DateString,
        item.Contact.Name,
        docType === "creditNotes" ? item.CreditNoteNumber : item.InvoiceNumber,
        item.Type,
        item.Status,
        docType === "creditNotes" ? item.CreditNoteID : item.InvoiceID,
        "attachment",
        fileName
      );

      await xero.creditNotes.attachments.downloadAttachment({
        entityId:
          docType === "creditNotes" ? item.CreditNoteID : item.InvoiceID,
        mimeType,
        fileName,
        pathToSave: path.resolve(
          __dirname,
          `../bulk/${
            docType === "creditNotes" ? `creditnotes` : `invoice`
          }/${attachmentNameForBucket}.pdf`
        )
      });

      await uploadFilesToGCS(
        `${
          docType === "creditNotes" ? `creditnotes` : `invoice`
        }/${attachmentNameForBucket}.pdf`
      );
    });
  } catch (e) {
    console.log(e);
  }
};

const handleNotes = async (docs, docType, userStartDate, userEndDate, logStart) => {
  let cnt = 0;
  docs.forEach(async item => {
    const mainDate = new Date(item.DateString);
    if (
      mainDate.getTime() < userEndDate.getTime() &&
      mainDate.getTime() > userStartDate.getTime()
    ) {

      const nameForBucket = getFileNameForGoogleCloud(
        item.DateString,
        item.Contact.Name,
        docType === "creditNotes" ? item.CreditNoteNumber : item.InvoiceNumber,
        item.Type,
        item.Status,
        docType === "creditNotes" ? item.CreditNoteID : item.InvoiceID,
        null,
        null
      );
      cnt++;
      if (cnt > 30 && cnt % 30 === 0) {
        var waitTill = new Date(new Date().getTime() + 8000);
        while(waitTill > new Date()){}
      }
      saveLogFile(
        logStart,
        docType === "creditNotes" ? item.CreditNoteID : item.InvoiceID,
        1,
        item.Status
      )

      if (item.HasAttachments) {
        saveLogFile(
          logStart,
          docType === "creditNotes" ? item.CreditNoteID : item.InvoiceID,
          2,
          item.Status
        )
      }

      await saveDocumentFromXero(docType, item, nameForBucket)

      if (item.HasAttachments) {
        cnt++;
        await saveAttachmentsFromXero(docType, item);
      }
    }
  });
};

router.get("/", async (req, res) => {
  const { id, startDate, endDate } = req.query;
  const userStartDate = new Date(startDate);
  const userEndDate = new Date(endDate);

  if (id === "creditnotes") {
    try {
      const creditNotes = await xero.creditNotes.get();
      const { CreditNotes } = creditNotes;
      await handleNotes(CreditNotes, "creditNotes", userStartDate, userEndDate);

      res.status(200).send(CreditNotes);
    } catch (e) {
      res.status(404).send(e);
    }
  } else {
      try {
        const invoices = await xero.invoices.get();
        const { Invoices } = invoices;
        const nowTime = Date.now();
        const logStart = moment(nowTime).format("YYYY-MM-Do-HH-mm-ss-SSS");

        fs.writeFileSync(
          path.resolve(__dirname, `../bulk/logs/${logStart}.txt`),
          `Export Started\n${moment(nowTime).format("YYYY-MM-Do HH:mm:ss SSS")}\n\nDocuments For Export\n`,
          (err) => {
            if (err) return console.log(err)
          }
        );

        await handleNotes(Invoices, "invoice", userStartDate, userEndDate, logStart);
        
        fs.appendFileSync(
          path.resolve(__dirname, `../bulk/logs/${logStart}.txt`),
          `\nExport Ended\n${moment(Date.now()).format("YYYY-MM-Do HH:mm:ss SSS")}\n\nExport results\nSuccess`,
          (err) => {
            if (err) return console.log(err)
          }
        );
        await uploadFilesToGCS(`logs/${logStart}.txt`);
        res.status(200).send(Invoices);
      } catch (e) {
        res.status(e.statusCode).send(e);
      }
  }
});

module.exports = router;
