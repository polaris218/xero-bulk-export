const { Storage } = require("@google-cloud/storage");
const path = require("path");

const GOOGLE_CLOUD_PROJECT_ID = "xero-bulk-export";
const GOOGLE_CLOUD_KEYFILE = path.join(
  __dirname,
  "../config/xero-bulk-export-0471752b46ea.json"
);

const storage = new Storage({
  projectId: GOOGLE_CLOUD_PROJECT_ID,
  keyFilename: GOOGLE_CLOUD_KEYFILE
});

const bucketName = `xero-bulk-export`;

exports.getPublicUrl = (bucketName, fileName) =>
  `https://storage.googleapis.com/${bucketName}/${fileName}`;

exports.uploadFilesToGCS = async note => {
  const filePath = path.join(__dirname, `../bulk/${note}`);

  const bucket = storage.bucket(bucketName);
  const fileName = path.basename(filePath);
  const file = bucket.file(fileName);

  await bucket.upload(filePath, {
    destination: bucket.file(`${fileName}`)
  });
  console.log(`${fileName} is uploaded successfully`);
  // return bucket
  //   .upload(filePath, {
  //     destination: bucket.file(`${fileName}`)
  //   })
  //   .then(() => {
  //     console.log(`${fileName} is uploaded successfully`);
  //   });
};
