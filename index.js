/**
 * Responds to any HTTP request.
 *
 * @param {!express:Request} req HTTP request context.
 * @param {!express:Response} res HTTP response context.
 */
const express = require("express");
const bodyParser = require("body-parser");

const router = require("./controllers/routes");

const app = express();

app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use("/publish", router);

const port = process.env.PORT || 3000;

app.listen(port, () => {
  console.log(`server listening --> ${port}`);
});

module.exports = app;
